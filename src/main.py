import boto3

session = boto3.Session(region_name='eu-central-1',
                        aws_access_key_id='AKIAJHK4UKVNUFHNWRVQ',
                        aws_secret_access_key='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')

class DynamoDBUtils:
# get all item id in a table
    def __init__(self,tableName):
        self.tableName= tableName
        self.conn = session.resource('dynamodb')
        self.table = self.conn.Table(self.tableName)

    def getallItemsID(self):
        res = self.table.scan()
        id_list = [item['ID'] for item in res['Items']]
        return id_list

    def getallItemsIDandURL(self):
        res = self.table.scan()
        id_list = [(item['ID'],item['url'],item['name']) for item in res['Items']]
        return id_list

def send_message_to_queue (id, url,name):
    session.client('sqs').send_message(
        QueueUrl = "https://sqs.eu-central-1.amazonaws.com/419206837402/seeds_v1",
        MessageBody=str(url),
        MessageAttributes={
            'ID':{
                'DataType':'String',
                'StringValue': str(id)
            },
            'Name': {
                'DataType': 'String',
                'StringValue': str(name)
            }
        }
    )


def main(event,context):
    utils = DynamoDBUtils('seeds')
    id_list = utils.getallItemsIDandURL()

    for id,url,name in id_list:
        send_message_to_queue(id,url,name)
